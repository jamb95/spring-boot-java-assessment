package com.citi.training.assessment.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * This class is a model object to represent a row from the trade table. 
 * 
 * @author Baur Jambulov
 */
@Entity
public class TradeRow {
    
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
    
    private String stock;
    private int buy;
    private double price;
    private int volume;

    /**
     * Default constructor
     */
    public TradeRow() {}
    
    /**
     * Overloaded constructor with 5 parameters
     * 
     * @param id: initializes the 'id' property of a newly created TradeRow object
     * @param stock: initializes the 'stock' property of a newly created TradeRow object
     * @param buy: initializes the 'buy' property of a newly created TradeRow object
     * @param price: initializes the 'price' property of a newly created TradeRow object
     * @param volume: initializes the 'volume' property of a newly created TradeRow object
     */
    public TradeRow(long id, String stock, int buy, double price, int volume) {
    	this.id = id;
    	this.stock = stock;
    	this.buy = buy;
    	this.price = price;
    	this.volume = volume;
    }
    
    /**
     * @return this TradeRow's id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id: sets the 'id' property for this TradeRow
     */
    public void setId(long id) {
        this.id = id;
    }
    
    /**
     * @return this TradeRow's 'stock' property value
     */
	public String getStock() {
		return stock;
	}
	
    /**
     * @param stock: sets the 'stock' property for this TradeRow
     */
	public void setStock(String stock) {
		this.stock = stock;
	}
    
    /**
     * @return this TradeRow's 'buy' property value
     */
	public int getBuy() {
		return buy;
	}
	
    /**
     * @param buy: sets the 'buy' property for this TradeRow
     */
	public void setBuy(int buy) {
		this.buy = buy;
	}
	
    /**
     * @return this TradeRow's 'price' property value
     */
	public double getPrice() {
		return price;
	}
	
    /**
     * @param price: sets the 'price' property for this TradeRow
     */
	public void setPrice(double price) {
		this.price = price;
	}
	
    /**
     * @return this TradeRow's 'volume' property value
     */
	public int getVolume() {
		return volume;
	}
	
    /**
     * @param volume: sets the 'volume' property for this TradeRow
     */
	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	/**
	 * @return string representation of TradeRow object
	 */
    @Override
    public String toString() {
        return "TradeRow [id=" + id +
        		", stock=" + stock 
        		+ ", buy=" + buy 
        		+ ", price=" + price
        		+ ", volume=" + volume + "]";
    }

    /**
     * @param tradeRow: TradeRow object that is about to be compared
     * @return boolean value that is the result of TradeRow equality check
     */
    public boolean equals(TradeRow tradeRow) {
        return this.id == tradeRow.getId() &&
               this.stock.equals(tradeRow.getStock()) &&
               this.buy == tradeRow.getBuy() && 
               this.price == tradeRow.getPrice() &&
               this.volume == tradeRow.getVolume();
    }
    
}
