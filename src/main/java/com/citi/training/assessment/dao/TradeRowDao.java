package com.citi.training.assessment.dao;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.assessment.model.TradeRow;

/**
 * This interface allows us to run generic CRUD operations on a repository of 'TradeRow' class type
 * 
 * @author Baur Jambulov
 */
public interface TradeRowDao extends CrudRepository<TradeRow, Long> {

}
