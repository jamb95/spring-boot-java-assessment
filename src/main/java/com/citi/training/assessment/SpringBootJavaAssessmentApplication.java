package com.citi.training.assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * This class starts the project as a Spring Boot Application
 * 
 * @author Baur Jambulov
 */
@SpringBootApplication
public class SpringBootJavaAssessmentApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootJavaAssessmentApplication.class, args);
	}

}
