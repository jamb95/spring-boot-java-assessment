package com.citi.training.assessment.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.assessment.dao.TradeRowDao;
import com.citi.training.assessment.model.TradeRow;

/**
 * This class represents the Spring-type REST controller with base mapping '/traderows'
 * 
 * @author Baur Jambulov
 */
@RestController
@RequestMapping("/traderows")
public class TradeRowController {
	
	private static final Logger LOG = LoggerFactory.getLogger(TradeRowController.class);
			
	@Autowired
	private TradeRowDao tradeRowDao;
	
	/**
	 * @return an object that can be target of the "for-each loop" statement
	 */
    @RequestMapping(method=RequestMethod.GET)
    public Iterable<TradeRow> findAll() {
        LOG.info("HTTP GET findAll()");
        return tradeRowDao.findAll();
    }

    /**
     * @param id: id of the TradeRow object that needs to be searched
     * @return TradeRow object
     */
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public TradeRow findById(@PathVariable long id) {
        LOG.info("HTTP GET findById() id=[" + id + "]");
        return tradeRowDao.findById(id).get();
    }

    /**
     * @param tradeRow: TradeRow object that needs to be created
     * @return an object that represents an HTTP request or response entity, consisting of headers and body
     */
    @RequestMapping(method=RequestMethod.POST)
    public HttpEntity<TradeRow> save(@RequestBody TradeRow tradeRow) {
        LOG.info("HTTP POST save() trade row=[" + tradeRow + "]");
        
        if (tradeRow.getBuy() < 0 || tradeRow.getBuy() > 1) {
        	throw new IllegalArgumentException("Buy property should be either 0 or 1!");
        }
        
        return new ResponseEntity<TradeRow>(tradeRowDao.save(tradeRow), HttpStatus.CREATED);
    }

    /**
     * @param id: id of the TradeRow object that needs to be deleted
     */
    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value=HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id) {
        LOG.info("HTTP DELETE delete() id=[" + id + "]");
        tradeRowDao.deleteById(id);
    }
    
}
