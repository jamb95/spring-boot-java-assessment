package com.citi.training.assessment.rest;

import java.util.NoSuchElementException;

import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * This class ensures that the API returns correct RESTful HTTP response codes
 * 
 * @author Baur Jambulov
 */
@ControllerAdvice
@Priority(1)
public class DefaultExceptionHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);
	   
	/**
	 * @param request: Incoming request from the client
	 * @param ex: Exception handler of type 'Exception' - provides generic approach
	 * @return Extension of {@link HttpEntity} that adds a {@link HttpStatus} status code
	 */
    @ExceptionHandler(value = {NoSuchElementException.class, EmptyResultDataAccessException.class})
    public ResponseEntity<Object> tradeRowNotFoundExceptionHandler(
            HttpServletRequest request, Exception ex) {
        logger.warn(ex.toString());
        return new ResponseEntity<>("{\"message\": \"" + ex.getMessage() + "\"}",
                                    HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(value = {IllegalArgumentException.class})
    public ResponseEntity<Object> tradeRowIllegalBuyAction(
            HttpServletRequest request, Exception ex) {
        logger.warn(ex.toString());
        return new ResponseEntity<>("{\"message\": \"" + ex.getMessage() + "\"}",
                                    HttpStatus.BAD_REQUEST);
    }
}
