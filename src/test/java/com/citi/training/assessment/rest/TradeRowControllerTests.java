package com.citi.training.assessment.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.assessment.dao.TradeRowDao;
import com.citi.training.assessment.model.TradeRow;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(TradeRowController.class)
public class TradeRowControllerTests {

	private static final Logger logger = LoggerFactory.getLogger(TradeRowControllerTests.class);
	
    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeRowDao tradeRowDao;
    
    @Test
    public void findAllTradeRows_returnsList() throws Exception {
        when(tradeRowDao.findAll()).thenReturn(new ArrayList<TradeRow>());

        MvcResult result = this.mockMvc.perform(get("/traderows")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();

        logger.info("Result from TradeRowDao.findAll: " +
                    result.getResponse().getContentAsString());
    }


    @Test
    public void getTadeRowById_returnsOK() throws Exception {
    	TradeRow testTradeRow = new TradeRow(105, "C", 1, 7.68, 15000);

        when(tradeRowDao.findById(testTradeRow.getId())).thenReturn(
                                                    Optional.of(testTradeRow));

        MvcResult result = this.mockMvc.perform(get("/traderows/" + testTradeRow.getId()))
                                       .andExpect(status().isOk()).andReturn();

        logger.info("Result from StockDao.getStock: " +
                    result.getResponse().getContentAsString());
    }
    
}
