package com.citi.training.assessment.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.assessment.model.TradeRow;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradeRowControllerIntegrationTests {

	private static final Logger LOG = LoggerFactory.getLogger(TradeRowControllerIntegrationTests.class);
			
    @Autowired
    private TestRestTemplate restTemplate;
    
    @Test
    public void getStock_returnsStock() {
    	String testStock = "AAPL";
    	int testBuy = 1;
    	double testPrice = 107.89;
    	int testVolume = 104890;
    	
        // Do a HTTP POST to /traderows
        ResponseEntity<TradeRow> createTradeRowResponse = restTemplate.postForEntity("/traderows",
                                             new TradeRow(-1, testStock, testBuy, testPrice, testVolume),
                                             TradeRow.class);

        LOG.info("Create Trade Row response: " + createTradeRowResponse.getBody());
        assertEquals(HttpStatus.CREATED, createTradeRowResponse.getStatusCode());
        assertEquals("created Trade Row ticker should equal testStock",
        		testStock, createTradeRowResponse.getBody().getStock());
        assertEquals("created Trade Row buy should equal testBuy",
        		testBuy, createTradeRowResponse.getBody().getBuy());
        assertEquals("created Trade Row price should equal testPrice",
        		testPrice, createTradeRowResponse.getBody().getPrice(), 0.001);
        assertEquals("created Trade Row volume should equal testVolume",
        		testVolume, createTradeRowResponse.getBody().getVolume());

        
        // Do a HTTP GET to /traderows/ID
        long dbId = createTradeRowResponse.getBody().getId();
        ResponseEntity<TradeRow> getTradeRowResponse = restTemplate.getForEntity("/traderows/" + dbId,
        		TradeRow.class);
        
        LOG.info("Get Trade Row response: " + getTradeRowResponse.getBody());
        assertEquals(HttpStatus.OK, getTradeRowResponse.getStatusCode());
        assertEquals("retrieved Trade Row ticker should equal testStock",
        		testStock, getTradeRowResponse.getBody().getStock());
        assertEquals("retrieved Trade Row buy should equal testBuy",
        		testBuy, getTradeRowResponse.getBody().getBuy());
        assertEquals("retrieved Trade Row price should equal testPrice",
        		testPrice, getTradeRowResponse.getBody().getPrice(), 0.001);
        assertEquals("retrieved Trade Row volume should equal testVolume",
        		testVolume, getTradeRowResponse.getBody().getVolume());
        
        
        // Do a HTTP DELETE to /traderows/ID
        ResponseEntity<TradeRow> deleteTradeRowResponse = restTemplate.exchange(
        		"/traderows/" + dbId, 
        		HttpMethod.DELETE, 
        		null, 
        		new ParameterizedTypeReference<TradeRow>(){});
        
        LOG.info("Delete Trade Row response: " + deleteTradeRowResponse.getBody());
        assertEquals(HttpStatus.NO_CONTENT, deleteTradeRowResponse.getStatusCode());
    }
    
}
