package com.citi.training.assessment.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TradeRowTests {
	
	private long testId = 7;
	private String testStock = "AAPL";
	private int testBuy = 1;
	private double testPrice = 107.89;
	private int testVolume = 104890;
	
    @Test
    public void test_TradeRow_defaultConstructorAndSetters() {
        TradeRow testTradeRow = new TradeRow();

        testTradeRow.setId(testId);
        testTradeRow.setStock(testStock);
        testTradeRow.setBuy(testBuy);
        testTradeRow.setPrice(testPrice);
        testTradeRow.setVolume(testVolume);

        assertEquals("TradeRow id should be equal to testId",
        		testId, testTradeRow.getId());
        assertEquals("TradeRow stock should be equal to testStock",
        		testStock, testTradeRow.getStock());
        assertEquals("TradeRow buy should be equal to testBuy",
        		testBuy, testTradeRow.getBuy());
        assertEquals("TradeRow price should be equal to testPrice",
        		testPrice, testTradeRow.getPrice(), 0.001);
        assertEquals("TradeRow volume should be equal to testVolume",
        		testVolume, testTradeRow.getVolume());
    }

    @Test
    public void test_TradeRow_fullConstructor() {
    	TradeRow testTradeRow = new TradeRow(testId, testStock, testBuy, testPrice, testVolume);

        assertEquals("TradeRow id should be equal to testId",
        		testId, testTradeRow.getId());
        assertEquals("TradeRow stock should be equal to testStock",
        		testStock, testTradeRow.getStock());
        assertEquals("TradeRow buy should be equal to testBuy",
        		testBuy, testTradeRow.getBuy());
        assertEquals("TradeRow price should be equal to testPrice",
        		testPrice, testTradeRow.getPrice(), 0.001);
        assertEquals("TradeRow volume should be equal to testVolume",
        		testVolume, testTradeRow.getVolume());
    }

    @Test
    public void test_TradeRow_toString() {
    	TradeRow testTradeRow = new TradeRow(testId, testStock, testBuy, testPrice, testVolume);

        assertTrue("toString should contain testId",
        		testTradeRow.toString().contains(Long.toString(testId)));
        assertTrue("toString should contain testStock",
        		testTradeRow.toString().contains(testStock));
        assertTrue("toString should contain testBuy",
        		testTradeRow.toString().contains(Integer.toString(testBuy)));
        assertTrue("toString should contain testPrice",
        		testTradeRow.toString().contains(Double.toString(testPrice)));
        assertTrue("toString should contain testVolume",
        		testTradeRow.toString().contains(Integer.toString(testVolume)));
    }

    @Test
    public void test_TradeRow_equals() {
    	TradeRow testTradeRow1 = new TradeRow(testId, testStock, testBuy, testPrice, testVolume);
    	TradeRow testTradeRow2 = new TradeRow(testId, testStock, testBuy, testPrice, testVolume);
    	
        assertFalse("TradeRow objects for test should not be the same object",
        		testTradeRow1 == testTradeRow2);
        assertTrue("TradeRow objects for test should be found to be equal",
        		testTradeRow1.equals(testTradeRow2));
    }

    @Test
    public void test_TradeRow_equalsFails() {
    	TradeRow testTradeRow1 = new TradeRow(testId + 1, testStock, testBuy, testPrice, testVolume);
    	TradeRow testTradeRow2 = new TradeRow(testId, testStock + "test", testBuy, testPrice, testVolume);
    	TradeRow testTradeRow3 = new TradeRow(testId, testStock, testBuy - 1, testPrice, testVolume);
    	TradeRow testTradeRow4 = new TradeRow(testId, testStock, testBuy - 1, testPrice + 200, testVolume);
    	TradeRow testTradeRow5 = new TradeRow(testId, testStock, testBuy - 1, testPrice, testVolume - 150);
    	TradeRow[] createdTradeRows = {testTradeRow1, testTradeRow2, testTradeRow3, testTradeRow4, testTradeRow5};
    	
    	TradeRow compareTradeRow = new TradeRow(testId, testStock, testBuy, testPrice, testVolume);

        for(TradeRow thisTradeRow : createdTradeRows) {
            assertFalse("TradeRow objects for test should not be the same object",
            		thisTradeRow == compareTradeRow);
            assertFalse("TradeRow objects for test should be found to be equal",
            		thisTradeRow.equals(compareTradeRow));
        }
    }

}
